//
// Definitions for schema: http://www.universite.org/Interfaces/GestionInscriptions/v2
//  file:/C:/FormationMaster/workspace/TP4%20Client%20WS%20SOAP/src/resources/GestionInscriptions-2.0.wsdl#types1
//
//
// Constructor for XML Schema item {http://www.universite.org/Interfaces/GestionInscriptions/v2}inscrireEtudiant
//
function INTF_inscrireEtudiant () {
    this.typeMarker = 'INTF_inscrireEtudiant';
    this._etudiant = null;
}

//
// accessor is INTF_inscrireEtudiant.prototype.getEtudiant
// element get for etudiant
// - element type is {http://www.universite.org/Interfaces/Etudiant/v1/model}EtudiantType
// - required element
//
// element set for etudiant
// setter function is is INTF_inscrireEtudiant.prototype.setEtudiant
//
function INTF_inscrireEtudiant_getEtudiant() { return this._etudiant;}

INTF_inscrireEtudiant.prototype.getEtudiant = INTF_inscrireEtudiant_getEtudiant;

function INTF_inscrireEtudiant_setEtudiant(value) { this._etudiant = value;}

INTF_inscrireEtudiant.prototype.setEtudiant = INTF_inscrireEtudiant_setEtudiant;
//
// Serialize {http://www.universite.org/Interfaces/GestionInscriptions/v2}inscrireEtudiant
//
function INTF_inscrireEtudiant_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName !== null) {
     xml = xml + '<';
     xml = xml + elementName;
     xml = xml + ' ';
     xml = xml + 'xmlns:jns0=\'http://www.universite.org/Interfaces/GestionInscriptions/v2\' ';
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     xml = xml + this._etudiant.serialize(cxfjsutils, 'jns0:etudiant', null);
    }
    if (elementName !== null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

INTF_inscrireEtudiant.prototype.serialize = INTF_inscrireEtudiant_serialize;

function INTF_inscrireEtudiant_deserialize (cxfjsutils, element) {
    var newobject = new INTF_inscrireEtudiant();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing etudiant');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = ETUD_EtudiantType_deserialize(cxfjsutils, curElement);
    }
    newobject.setEtudiant(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    return newobject;
}

//
// Constructor for XML Schema item {http://www.universite.org/Interfaces/GestionInscriptions/v2}listerInscrits
//
function INTF_listerInscrits () {
    this.typeMarker = 'INTF_listerInscrits';
}

//
// Serialize {http://www.universite.org/Interfaces/GestionInscriptions/v2}listerInscrits
//
function INTF_listerInscrits_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName !== null) {
     xml = xml + '<';
     xml = xml + elementName;
     xml = xml + ' ';
     xml = xml + 'xmlns:jns0=\'http://www.universite.org/Interfaces/GestionInscriptions/v2\' ';
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    if (elementName !== null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

INTF_listerInscrits.prototype.serialize = INTF_listerInscrits_serialize;

function INTF_listerInscrits_deserialize (cxfjsutils, element) {
    var newobject = new INTF_listerInscrits();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    return newobject;
}

//
// Constructor for XML Schema item {http://www.universite.org/Interfaces/GestionInscriptions/v2}etudiantInexistant
//
function INTF_etudiantInexistant () {
    this.typeMarker = 'INTF_etudiantInexistant';
    this._classe = '';
    this._methode = '';
    this._raison = '';
    this._description = null;
}

//
// accessor is INTF_etudiantInexistant.prototype.getClasse
// element get for classe
// - element type is {http://www.w3.org/2001/XMLSchema}string
// - required element
//
// element set for classe
// setter function is is INTF_etudiantInexistant.prototype.setClasse
//
function INTF_etudiantInexistant_getClasse() { return this._classe;}

INTF_etudiantInexistant.prototype.getClasse = INTF_etudiantInexistant_getClasse;

function INTF_etudiantInexistant_setClasse(value) { this._classe = value;}

INTF_etudiantInexistant.prototype.setClasse = INTF_etudiantInexistant_setClasse;
//
// accessor is INTF_etudiantInexistant.prototype.getMethode
// element get for methode
// - element type is {http://www.w3.org/2001/XMLSchema}string
// - required element
//
// element set for methode
// setter function is is INTF_etudiantInexistant.prototype.setMethode
//
function INTF_etudiantInexistant_getMethode() { return this._methode;}

INTF_etudiantInexistant.prototype.getMethode = INTF_etudiantInexistant_getMethode;

function INTF_etudiantInexistant_setMethode(value) { this._methode = value;}

INTF_etudiantInexistant.prototype.setMethode = INTF_etudiantInexistant_setMethode;
//
// accessor is INTF_etudiantInexistant.prototype.getRaison
// element get for raison
// - element type is {http://www.w3.org/2001/XMLSchema}string
// - required element
//
// element set for raison
// setter function is is INTF_etudiantInexistant.prototype.setRaison
//
function INTF_etudiantInexistant_getRaison() { return this._raison;}

INTF_etudiantInexistant.prototype.getRaison = INTF_etudiantInexistant_getRaison;

function INTF_etudiantInexistant_setRaison(value) { this._raison = value;}

INTF_etudiantInexistant.prototype.setRaison = INTF_etudiantInexistant_setRaison;
//
// accessor is INTF_etudiantInexistant.prototype.getDescription
// element get for description
// - element type is {http://www.w3.org/2001/XMLSchema}string
// - optional element
//
// element set for description
// setter function is is INTF_etudiantInexistant.prototype.setDescription
//
function INTF_etudiantInexistant_getDescription() { return this._description;}

INTF_etudiantInexistant.prototype.getDescription = INTF_etudiantInexistant_getDescription;

function INTF_etudiantInexistant_setDescription(value) { this._description = value;}

INTF_etudiantInexistant.prototype.setDescription = INTF_etudiantInexistant_setDescription;
//
// Serialize {http://www.universite.org/Interfaces/GestionInscriptions/v2}etudiantInexistant
//
function INTF_etudiantInexistant_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName !== null) {
     xml = xml + '<';
     xml = xml + elementName;
     xml = xml + ' ';
     xml = xml + 'xmlns:jns0=\'http://www.universite.org/Interfaces/GestionInscriptions/v2\' xmlns:jns1=\'http://www.universite.org/Interfaces/exception\' ';
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     xml = xml + '<jns1:classe>';
     xml = xml + cxfjsutils.escapeXmlEntities(this._classe);
     xml = xml + '</jns1:classe>';
    }
    // block for local variables
    {
     xml = xml + '<jns1:methode>';
     xml = xml + cxfjsutils.escapeXmlEntities(this._methode);
     xml = xml + '</jns1:methode>';
    }
    // block for local variables
    {
     xml = xml + '<jns1:raison>';
     xml = xml + cxfjsutils.escapeXmlEntities(this._raison);
     xml = xml + '</jns1:raison>';
    }
    // block for local variables
    {
     if (this._description != null) {
      xml = xml + '<jns1:description>';
      xml = xml + cxfjsutils.escapeXmlEntities(this._description);
      xml = xml + '</jns1:description>';
     }
    }
    if (elementName !== null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

INTF_etudiantInexistant.prototype.serialize = INTF_etudiantInexistant_serialize;

function INTF_etudiantInexistant_deserialize (cxfjsutils, element) {
    var newobject = new INTF_etudiantInexistant();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing classe');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     value = cxfjsutils.getNodeText(curElement);
     item = value;
    }
    newobject.setClasse(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing methode');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     value = cxfjsutils.getNodeText(curElement);
     item = value;
    }
    newobject.setMethode(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing raison');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     value = cxfjsutils.getNodeText(curElement);
     item = value;
    }
    newobject.setRaison(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing description');
    if (curElement != null && cxfjsutils.isNodeNamedNS(curElement, 'http://www.universite.org/Interfaces/exception', 'description')) {
     var value = null;
     if (!cxfjsutils.isElementNil(curElement)) {
      value = cxfjsutils.getNodeText(curElement);
      item = value;
     }
     newobject.setDescription(item);
     var item = null;
     if (curElement != null) {
      curElement = cxfjsutils.getNextElementSibling(curElement);
     }
    }
    return newobject;
}

//
// Constructor for XML Schema item {http://www.universite.org/Interfaces/GestionInscriptions/v2}InfosEtudiantIncorrect
//
function INTF_InfosEtudiantIncorrect () {
    this.typeMarker = 'INTF_InfosEtudiantIncorrect';
    this._classe = '';
    this._methode = '';
    this._raison = '';
    this._description = null;
    this._champsErronnes = [];
}

//
// accessor is INTF_InfosEtudiantIncorrect.prototype.getClasse
// element get for classe
// - element type is {http://www.w3.org/2001/XMLSchema}string
// - required element
//
// element set for classe
// setter function is is INTF_InfosEtudiantIncorrect.prototype.setClasse
//
function INTF_InfosEtudiantIncorrect_getClasse() { return this._classe;}

INTF_InfosEtudiantIncorrect.prototype.getClasse = INTF_InfosEtudiantIncorrect_getClasse;

function INTF_InfosEtudiantIncorrect_setClasse(value) { this._classe = value;}

INTF_InfosEtudiantIncorrect.prototype.setClasse = INTF_InfosEtudiantIncorrect_setClasse;
//
// accessor is INTF_InfosEtudiantIncorrect.prototype.getMethode
// element get for methode
// - element type is {http://www.w3.org/2001/XMLSchema}string
// - required element
//
// element set for methode
// setter function is is INTF_InfosEtudiantIncorrect.prototype.setMethode
//
function INTF_InfosEtudiantIncorrect_getMethode() { return this._methode;}

INTF_InfosEtudiantIncorrect.prototype.getMethode = INTF_InfosEtudiantIncorrect_getMethode;

function INTF_InfosEtudiantIncorrect_setMethode(value) { this._methode = value;}

INTF_InfosEtudiantIncorrect.prototype.setMethode = INTF_InfosEtudiantIncorrect_setMethode;
//
// accessor is INTF_InfosEtudiantIncorrect.prototype.getRaison
// element get for raison
// - element type is {http://www.w3.org/2001/XMLSchema}string
// - required element
//
// element set for raison
// setter function is is INTF_InfosEtudiantIncorrect.prototype.setRaison
//
function INTF_InfosEtudiantIncorrect_getRaison() { return this._raison;}

INTF_InfosEtudiantIncorrect.prototype.getRaison = INTF_InfosEtudiantIncorrect_getRaison;

function INTF_InfosEtudiantIncorrect_setRaison(value) { this._raison = value;}

INTF_InfosEtudiantIncorrect.prototype.setRaison = INTF_InfosEtudiantIncorrect_setRaison;
//
// accessor is INTF_InfosEtudiantIncorrect.prototype.getDescription
// element get for description
// - element type is {http://www.w3.org/2001/XMLSchema}string
// - optional element
//
// element set for description
// setter function is is INTF_InfosEtudiantIncorrect.prototype.setDescription
//
function INTF_InfosEtudiantIncorrect_getDescription() { return this._description;}

INTF_InfosEtudiantIncorrect.prototype.getDescription = INTF_InfosEtudiantIncorrect_getDescription;

function INTF_InfosEtudiantIncorrect_setDescription(value) { this._description = value;}

INTF_InfosEtudiantIncorrect.prototype.setDescription = INTF_InfosEtudiantIncorrect_setDescription;
//
// accessor is INTF_InfosEtudiantIncorrect.prototype.getChampsErronnes
// element get for champsErronnes
// - element type is {http://www.w3.org/2001/XMLSchema}string
// - required element
// - array
//
// element set for champsErronnes
// setter function is is INTF_InfosEtudiantIncorrect.prototype.setChampsErronnes
//
function INTF_InfosEtudiantIncorrect_getChampsErronnes() { return this._champsErronnes;}

INTF_InfosEtudiantIncorrect.prototype.getChampsErronnes = INTF_InfosEtudiantIncorrect_getChampsErronnes;

function INTF_InfosEtudiantIncorrect_setChampsErronnes(value) { this._champsErronnes = value;}

INTF_InfosEtudiantIncorrect.prototype.setChampsErronnes = INTF_InfosEtudiantIncorrect_setChampsErronnes;
//
// Serialize {http://www.universite.org/Interfaces/GestionInscriptions/v2}InfosEtudiantIncorrect
//
function INTF_InfosEtudiantIncorrect_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName !== null) {
     xml = xml + '<';
     xml = xml + elementName;
     xml = xml + ' ';
     xml = xml + 'xmlns:jns0=\'http://www.universite.org/Interfaces/GestionInscriptions/v2\' xmlns:jns1=\'http://www.universite.org/Interfaces/exception\' ';
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     xml = xml + '<jns1:classe>';
     xml = xml + cxfjsutils.escapeXmlEntities(this._classe);
     xml = xml + '</jns1:classe>';
    }
    // block for local variables
    {
     xml = xml + '<jns1:methode>';
     xml = xml + cxfjsutils.escapeXmlEntities(this._methode);
     xml = xml + '</jns1:methode>';
    }
    // block for local variables
    {
     xml = xml + '<jns1:raison>';
     xml = xml + cxfjsutils.escapeXmlEntities(this._raison);
     xml = xml + '</jns1:raison>';
    }
    // block for local variables
    {
     if (this._description != null) {
      xml = xml + '<jns1:description>';
      xml = xml + cxfjsutils.escapeXmlEntities(this._description);
      xml = xml + '</jns1:description>';
     }
    }
    // block for local variables
    {
     if (this._champsErronnes != null) {
      for (var ax = 0;ax < this._champsErronnes.length;ax ++) {
       if (this._champsErronnes[ax] == null) {
        xml = xml + '<jns0:champsErronnes/>';
       } else {
        xml = xml + '<jns0:champsErronnes>';
        xml = xml + cxfjsutils.escapeXmlEntities(this._champsErronnes[ax]);
        xml = xml + '</jns0:champsErronnes>';
       }
      }
     }
    }
    if (elementName !== null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

INTF_InfosEtudiantIncorrect.prototype.serialize = INTF_InfosEtudiantIncorrect_serialize;

function INTF_InfosEtudiantIncorrect_deserialize (cxfjsutils, element) {
    var newobject = new INTF_InfosEtudiantIncorrect();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing classe');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     value = cxfjsutils.getNodeText(curElement);
     item = value;
    }
    newobject.setClasse(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing methode');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     value = cxfjsutils.getNodeText(curElement);
     item = value;
    }
    newobject.setMethode(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing raison');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     value = cxfjsutils.getNodeText(curElement);
     item = value;
    }
    newobject.setRaison(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing description');
    if (curElement != null && cxfjsutils.isNodeNamedNS(curElement, 'http://www.universite.org/Interfaces/exception', 'description')) {
     var value = null;
     if (!cxfjsutils.isElementNil(curElement)) {
      value = cxfjsutils.getNodeText(curElement);
      item = value;
     }
     newobject.setDescription(item);
     var item = null;
     if (curElement != null) {
      curElement = cxfjsutils.getNextElementSibling(curElement);
     }
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing champsErronnes');
    if (curElement != null && cxfjsutils.isNodeNamedNS(curElement, 'http://www.universite.org/Interfaces/GestionInscriptions/v2', 'champsErronnes')) {
     item = [];
     do  {
      var arrayItem = null;
      var value = null;
      if (!cxfjsutils.isElementNil(curElement)) {
       value = cxfjsutils.getNodeText(curElement);
       arrayItem = value;
      }
      item.push(arrayItem);
      curElement = cxfjsutils.getNextElementSibling(curElement);
     }
       while(curElement != null && cxfjsutils.isNodeNamedNS(curElement, 'http://www.universite.org/Interfaces/GestionInscriptions/v2', 'champsErronnes'));
     newobject.setChampsErronnes(item);
     var item = null;
    }
    return newobject;
}

//
// Constructor for XML Schema item {http://www.universite.org/Interfaces/GestionInscriptions/v2}listerInscritsReponse
//
function INTF_listerInscritsReponse () {
    this.typeMarker = 'INTF_listerInscritsReponse';
    this._inscrit = [];
}

//
// accessor is INTF_listerInscritsReponse.prototype.getInscrit
// element get for inscrit
// - element type is {http://www.universite.org/Interfaces/Etudiant/v1/model}EtudiantType
// - required element
// - array
//
// element set for inscrit
// setter function is is INTF_listerInscritsReponse.prototype.setInscrit
//
function INTF_listerInscritsReponse_getInscrit() { return this._inscrit;}

INTF_listerInscritsReponse.prototype.getInscrit = INTF_listerInscritsReponse_getInscrit;

function INTF_listerInscritsReponse_setInscrit(value) { this._inscrit = value;}

INTF_listerInscritsReponse.prototype.setInscrit = INTF_listerInscritsReponse_setInscrit;
//
// Serialize {http://www.universite.org/Interfaces/GestionInscriptions/v2}listerInscritsReponse
//
function INTF_listerInscritsReponse_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName !== null) {
     xml = xml + '<';
     xml = xml + elementName;
     xml = xml + ' ';
     xml = xml + 'xmlns:jns0=\'http://www.universite.org/Interfaces/GestionInscriptions/v2\' xmlns:jns1=\'http://www.universite.org/Interfaces/exception\' ';
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     if (this._inscrit != null) {
      for (var ax = 0;ax < this._inscrit.length;ax ++) {
       if (this._inscrit[ax] == null) {
        xml = xml + '<jns0:inscrit/>';
       } else {
        xml = xml + this._inscrit[ax].serialize(cxfjsutils, 'jns0:inscrit', null);
       }
      }
     }
    }
    if (elementName !== null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

INTF_listerInscritsReponse.prototype.serialize = INTF_listerInscritsReponse_serialize;

function INTF_listerInscritsReponse_deserialize (cxfjsutils, element) {
    var newobject = new INTF_listerInscritsReponse();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing inscrit');
    if (curElement != null && cxfjsutils.isNodeNamedNS(curElement, 'http://www.universite.org/Interfaces/GestionInscriptions/v2', 'inscrit')) {
     item = [];
     do  {
      var arrayItem = null;
      var value = null;
      if (!cxfjsutils.isElementNil(curElement)) {
       arrayItem = ETUD_EtudiantType_deserialize(cxfjsutils, curElement);
      }
      item.push(arrayItem);
      curElement = cxfjsutils.getNextElementSibling(curElement);
     }
       while(curElement != null && cxfjsutils.isNodeNamedNS(curElement, 'http://www.universite.org/Interfaces/GestionInscriptions/v2', 'inscrit'));
     newobject.setInscrit(item);
     var item = null;
    }
    return newobject;
}

//
// Constructor for XML Schema item {http://www.universite.org/Interfaces/GestionInscriptions/v2}inscrireEtudiantReponse
//
function INTF_inscrireEtudiantReponse () {
    this.typeMarker = 'INTF_inscrireEtudiantReponse';
}

//
// Serialize {http://www.universite.org/Interfaces/GestionInscriptions/v2}inscrireEtudiantReponse
//
function INTF_inscrireEtudiantReponse_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName !== null) {
     xml = xml + '<';
     xml = xml + elementName;
     xml = xml + ' ';
     xml = xml + 'xmlns:jns0=\'http://www.universite.org/Interfaces/GestionInscriptions/v2\' xmlns:jns1=\'http://www.universite.org/Interfaces/exception\' ';
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    if (elementName !== null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

INTF_inscrireEtudiantReponse.prototype.serialize = INTF_inscrireEtudiantReponse_serialize;

function INTF_inscrireEtudiantReponse_deserialize (cxfjsutils, element) {
    var newobject = new INTF_inscrireEtudiantReponse();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    return newobject;
}

//
// Constructor for XML Schema item {http://www.universite.org/Interfaces/GestionInscriptions/v2}annulerInscription
//
function INTF_annulerInscription () {
    this.typeMarker = 'INTF_annulerInscription';
    this._id = null;
    this._Nom = null;
}

//
// accessor is INTF_annulerInscription.prototype.getId
// element get for id
// - element type is {http://www.w3.org/2001/XMLSchema}string
// - optional element
//
// element set for id
// setter function is is INTF_annulerInscription.prototype.setId
//
function INTF_annulerInscription_getId() { return this._id;}

INTF_annulerInscription.prototype.getId = INTF_annulerInscription_getId;

function INTF_annulerInscription_setId(value) { this._id = value;}

INTF_annulerInscription.prototype.setId = INTF_annulerInscription_setId;
//
// accessor is INTF_annulerInscription.prototype.getNom
// element get for Nom
// - element type is {http://www.w3.org/2001/XMLSchema}string
// - optional element
//
// element set for Nom
// setter function is is INTF_annulerInscription.prototype.setNom
//
function INTF_annulerInscription_getNom() { return this._Nom;}

INTF_annulerInscription.prototype.getNom = INTF_annulerInscription_getNom;

function INTF_annulerInscription_setNom(value) { this._Nom = value;}

INTF_annulerInscription.prototype.setNom = INTF_annulerInscription_setNom;
//
// Serialize {http://www.universite.org/Interfaces/GestionInscriptions/v2}annulerInscription
//
function INTF_annulerInscription_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName !== null) {
     xml = xml + '<';
     xml = xml + elementName;
     xml = xml + ' ';
     xml = xml + 'xmlns:jns0=\'http://www.universite.org/Interfaces/GestionInscriptions/v2\' xmlns:jns1=\'http://www.universite.org/Interfaces/exception\' ';
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     if (this._id != null) {
      xml = xml + '<jns0:id>';
      xml = xml + cxfjsutils.escapeXmlEntities(this._id);
      xml = xml + '</jns0:id>';
     }
    }
    // block for local variables
    {
     if (this._Nom != null) {
      xml = xml + '<jns0:Nom>';
      xml = xml + cxfjsutils.escapeXmlEntities(this._Nom);
      xml = xml + '</jns0:Nom>';
     }
    }
    if (elementName !== null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

INTF_annulerInscription.prototype.serialize = INTF_annulerInscription_serialize;

function INTF_annulerInscription_deserialize (cxfjsutils, element) {
    var newobject = new INTF_annulerInscription();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing id');
    if (curElement != null && cxfjsutils.isNodeNamedNS(curElement, 'http://www.universite.org/Interfaces/GestionInscriptions/v2', 'id')) {
     var value = null;
     if (!cxfjsutils.isElementNil(curElement)) {
      value = cxfjsutils.getNodeText(curElement);
      item = value;
     }
     newobject.setId(item);
     var item = null;
     if (curElement != null) {
      curElement = cxfjsutils.getNextElementSibling(curElement);
     }
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing Nom');
    if (curElement != null && cxfjsutils.isNodeNamedNS(curElement, 'http://www.universite.org/Interfaces/GestionInscriptions/v2', 'Nom')) {
     var value = null;
     if (!cxfjsutils.isElementNil(curElement)) {
      value = cxfjsutils.getNodeText(curElement);
      item = value;
     }
     newobject.setNom(item);
     var item = null;
     if (curElement != null) {
      curElement = cxfjsutils.getNextElementSibling(curElement);
     }
    }
    return newobject;
}

//
// Constructor for XML Schema item {http://www.universite.org/Interfaces/GestionInscriptions/v2}annulerInscriptionReponse
//
function INTF_annulerInscriptionReponse () {
    this.typeMarker = 'INTF_annulerInscriptionReponse';
}

//
// Serialize {http://www.universite.org/Interfaces/GestionInscriptions/v2}annulerInscriptionReponse
//
function INTF_annulerInscriptionReponse_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName !== null) {
     xml = xml + '<';
     xml = xml + elementName;
     xml = xml + ' ';
     xml = xml + 'xmlns:jns0=\'http://www.universite.org/Interfaces/GestionInscriptions/v2\' xmlns:jns1=\'http://www.universite.org/Interfaces/exception\' ';
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    if (elementName !== null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

INTF_annulerInscriptionReponse.prototype.serialize = INTF_annulerInscriptionReponse_serialize;

function INTF_annulerInscriptionReponse_deserialize (cxfjsutils, element) {
    var newobject = new INTF_annulerInscriptionReponse();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    return newobject;
}

//
// Definitions for schema: http://www.universite.org/Interfaces/Etudiant/v1/model
//  file:/C:/FormationMaster/workspace/TP4%20Client%20WS%20SOAP/src/resources/etudiant.xsd
//
//
// Simple type (enumeration) {http://www.universite.org/Interfaces/Etudiant/v1/model}VoieType
//
// - Rue
// - Avenue
// - Boulevard
// - Impasse
//
// Constructor for XML Schema item {http://www.universite.org/Interfaces/Etudiant/v1/model}EtudiantType
//
function ETUD_EtudiantType () {
    this.typeMarker = 'ETUD_EtudiantType';
    this._Identifiant = '';
    this._Prenom = '';
    this._Nom = '';
    this._Adresse = null;
}

//
// accessor is ETUD_EtudiantType.prototype.getIdentifiant
// element get for Identifiant
// - element type is {http://www.w3.org/2001/XMLSchema}string
// - required element
//
// element set for Identifiant
// setter function is is ETUD_EtudiantType.prototype.setIdentifiant
//
function ETUD_EtudiantType_getIdentifiant() { return this._Identifiant;}

ETUD_EtudiantType.prototype.getIdentifiant = ETUD_EtudiantType_getIdentifiant;

function ETUD_EtudiantType_setIdentifiant(value) { this._Identifiant = value;}

ETUD_EtudiantType.prototype.setIdentifiant = ETUD_EtudiantType_setIdentifiant;
//
// accessor is ETUD_EtudiantType.prototype.getPrenom
// element get for Prenom
// - element type is {http://www.w3.org/2001/XMLSchema}string
// - required element
//
// element set for Prenom
// setter function is is ETUD_EtudiantType.prototype.setPrenom
//
function ETUD_EtudiantType_getPrenom() { return this._Prenom;}

ETUD_EtudiantType.prototype.getPrenom = ETUD_EtudiantType_getPrenom;

function ETUD_EtudiantType_setPrenom(value) { this._Prenom = value;}

ETUD_EtudiantType.prototype.setPrenom = ETUD_EtudiantType_setPrenom;
//
// accessor is ETUD_EtudiantType.prototype.getNom
// element get for Nom
// - element type is {http://www.w3.org/2001/XMLSchema}string
// - required element
//
// element set for Nom
// setter function is is ETUD_EtudiantType.prototype.setNom
//
function ETUD_EtudiantType_getNom() { return this._Nom;}

ETUD_EtudiantType.prototype.getNom = ETUD_EtudiantType_getNom;

function ETUD_EtudiantType_setNom(value) { this._Nom = value;}

ETUD_EtudiantType.prototype.setNom = ETUD_EtudiantType_setNom;
//
// accessor is ETUD_EtudiantType.prototype.getAdresse
// element get for Adresse
// - element type is {http://www.universite.org/Interfaces/Etudiant/v1/model}AdresseType
// - required element
//
// element set for Adresse
// setter function is is ETUD_EtudiantType.prototype.setAdresse
//
function ETUD_EtudiantType_getAdresse() { return this._Adresse;}

ETUD_EtudiantType.prototype.getAdresse = ETUD_EtudiantType_getAdresse;

function ETUD_EtudiantType_setAdresse(value) { this._Adresse = value;}

ETUD_EtudiantType.prototype.setAdresse = ETUD_EtudiantType_setAdresse;
//
// Serialize {http://www.universite.org/Interfaces/Etudiant/v1/model}EtudiantType
//
function ETUD_EtudiantType_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName !== null) {
     xml = xml + '<';
     xml = xml + elementName;
     xml = xml + ' ';
     xml = xml + 'xmlns:jns0=\'http://www.universite.org/Interfaces/GestionInscriptions/v2\' xmlns:jns1=\'http://www.universite.org/Interfaces/exception\' xmlns:jns2=\'http://www.universite.org/Interfaces/Etudiant/v1/model\' ';
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     xml = xml + '<jns2:Identifiant>';
     xml = xml + cxfjsutils.escapeXmlEntities(this._Identifiant);
     xml = xml + '</jns2:Identifiant>';
    }
    // block for local variables
    {
     xml = xml + '<jns2:Prenom>';
     xml = xml + cxfjsutils.escapeXmlEntities(this._Prenom);
     xml = xml + '</jns2:Prenom>';
    }
    // block for local variables
    {
     xml = xml + '<jns2:Nom>';
     xml = xml + cxfjsutils.escapeXmlEntities(this._Nom);
     xml = xml + '</jns2:Nom>';
    }
    // block for local variables
    {
     xml = xml + this._Adresse.serialize(cxfjsutils, 'jns2:Adresse', null);
    }
    if (elementName !== null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

ETUD_EtudiantType.prototype.serialize = ETUD_EtudiantType_serialize;

function ETUD_EtudiantType_deserialize (cxfjsutils, element) {
    var newobject = new ETUD_EtudiantType();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing Identifiant');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     value = cxfjsutils.getNodeText(curElement);
     item = value;
    }
    newobject.setIdentifiant(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing Prenom');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     value = cxfjsutils.getNodeText(curElement);
     item = value;
    }
    newobject.setPrenom(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing Nom');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     value = cxfjsutils.getNodeText(curElement);
     item = value;
    }
    newobject.setNom(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing Adresse');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     item = ETUD_AdresseType_deserialize(cxfjsutils, curElement);
    }
    newobject.setAdresse(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    return newobject;
}

//
// Constructor for XML Schema item {http://www.universite.org/Interfaces/Etudiant/v1/model}AdresseType
//
function ETUD_AdresseType () {
    this.typeMarker = 'ETUD_AdresseType';
    this._numVoie = null;
    this._typeVoie = null;
    this._nomVoie = null;
    this._complement = null;
    this._codePostal = '';
    this._commune = null;
}

//
// accessor is ETUD_AdresseType.prototype.getNumVoie
// element get for numVoie
// - element type is {http://www.w3.org/2001/XMLSchema}positiveInteger
// - optional element
//
// element set for numVoie
// setter function is is ETUD_AdresseType.prototype.setNumVoie
//
function ETUD_AdresseType_getNumVoie() { return this._numVoie;}

ETUD_AdresseType.prototype.getNumVoie = ETUD_AdresseType_getNumVoie;

function ETUD_AdresseType_setNumVoie(value) { this._numVoie = value;}

ETUD_AdresseType.prototype.setNumVoie = ETUD_AdresseType_setNumVoie;
//
// accessor is ETUD_AdresseType.prototype.getTypeVoie
// element get for typeVoie
// - element type is {http://www.universite.org/Interfaces/Etudiant/v1/model}VoieType
// - optional element
//
// element set for typeVoie
// setter function is is ETUD_AdresseType.prototype.setTypeVoie
//
function ETUD_AdresseType_getTypeVoie() { return this._typeVoie;}

ETUD_AdresseType.prototype.getTypeVoie = ETUD_AdresseType_getTypeVoie;

function ETUD_AdresseType_setTypeVoie(value) { this._typeVoie = value;}

ETUD_AdresseType.prototype.setTypeVoie = ETUD_AdresseType_setTypeVoie;
//
// accessor is ETUD_AdresseType.prototype.getNomVoie
// element get for nomVoie
// - element type is {http://www.w3.org/2001/XMLSchema}string
// - optional element
//
// element set for nomVoie
// setter function is is ETUD_AdresseType.prototype.setNomVoie
//
function ETUD_AdresseType_getNomVoie() { return this._nomVoie;}

ETUD_AdresseType.prototype.getNomVoie = ETUD_AdresseType_getNomVoie;

function ETUD_AdresseType_setNomVoie(value) { this._nomVoie = value;}

ETUD_AdresseType.prototype.setNomVoie = ETUD_AdresseType_setNomVoie;
//
// accessor is ETUD_AdresseType.prototype.getComplement
// element get for complement
// - element type is {http://www.w3.org/2001/XMLSchema}string
// - optional element
//
// element set for complement
// setter function is is ETUD_AdresseType.prototype.setComplement
//
function ETUD_AdresseType_getComplement() { return this._complement;}

ETUD_AdresseType.prototype.getComplement = ETUD_AdresseType_getComplement;

function ETUD_AdresseType_setComplement(value) { this._complement = value;}

ETUD_AdresseType.prototype.setComplement = ETUD_AdresseType_setComplement;
//
// accessor is ETUD_AdresseType.prototype.getCodePostal
// element get for codePostal
// - element type is null
// - required element
//
// element set for codePostal
// setter function is is ETUD_AdresseType.prototype.setCodePostal
//
function ETUD_AdresseType_getCodePostal() { return this._codePostal;}

ETUD_AdresseType.prototype.getCodePostal = ETUD_AdresseType_getCodePostal;

function ETUD_AdresseType_setCodePostal(value) { this._codePostal = value;}

ETUD_AdresseType.prototype.setCodePostal = ETUD_AdresseType_setCodePostal;
//
// accessor is ETUD_AdresseType.prototype.getCommune
// element get for commune
// - element type is {http://www.w3.org/2001/XMLSchema}string
// - optional element
//
// element set for commune
// setter function is is ETUD_AdresseType.prototype.setCommune
//
function ETUD_AdresseType_getCommune() { return this._commune;}

ETUD_AdresseType.prototype.getCommune = ETUD_AdresseType_getCommune;

function ETUD_AdresseType_setCommune(value) { this._commune = value;}

ETUD_AdresseType.prototype.setCommune = ETUD_AdresseType_setCommune;
//
// Serialize {http://www.universite.org/Interfaces/Etudiant/v1/model}AdresseType
//
function ETUD_AdresseType_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName !== null) {
     xml = xml + '<';
     xml = xml + elementName;
     xml = xml + ' ';
     xml = xml + 'xmlns:jns0=\'http://www.universite.org/Interfaces/GestionInscriptions/v2\' xmlns:jns1=\'http://www.universite.org/Interfaces/exception\' xmlns:jns2=\'http://www.universite.org/Interfaces/Etudiant/v1/model\' ';
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     if (this._numVoie != null) {
      xml = xml + '<jns2:numVoie>';
      xml = xml + cxfjsutils.escapeXmlEntities(this._numVoie);
      xml = xml + '</jns2:numVoie>';
     }
    }
    // block for local variables
    {
     if (this._typeVoie != null) {
      xml = xml + '<jns2:typeVoie>';
      xml = xml + cxfjsutils.escapeXmlEntities(this._typeVoie);
      xml = xml + '</jns2:typeVoie>';
     }
    }
    // block for local variables
    {
     if (this._nomVoie != null) {
      xml = xml + '<jns2:nomVoie>';
      xml = xml + cxfjsutils.escapeXmlEntities(this._nomVoie);
      xml = xml + '</jns2:nomVoie>';
     }
    }
    // block for local variables
    {
     if (this._complement != null) {
      xml = xml + '<jns2:complement>';
      xml = xml + cxfjsutils.escapeXmlEntities(this._complement);
      xml = xml + '</jns2:complement>';
     }
    }
    // block for local variables
    {
     xml = xml + '<jns2:codePostal>';
     xml = xml + cxfjsutils.escapeXmlEntities(this._codePostal);
     xml = xml + '</jns2:codePostal>';
    }
    // block for local variables
    {
     if (this._commune != null) {
      xml = xml + '<jns2:commune>';
      xml = xml + cxfjsutils.escapeXmlEntities(this._commune);
      xml = xml + '</jns2:commune>';
     }
    }
    if (elementName !== null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

ETUD_AdresseType.prototype.serialize = ETUD_AdresseType_serialize;

function ETUD_AdresseType_deserialize (cxfjsutils, element) {
    var newobject = new ETUD_AdresseType();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing numVoie');
    if (curElement != null && cxfjsutils.isNodeNamedNS(curElement, 'http://www.universite.org/Interfaces/Etudiant/v1/model', 'numVoie')) {
     var value = null;
     if (!cxfjsutils.isElementNil(curElement)) {
      value = cxfjsutils.getNodeText(curElement);
      item = value;
     }
     newobject.setNumVoie(item);
     var item = null;
     if (curElement != null) {
      curElement = cxfjsutils.getNextElementSibling(curElement);
     }
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing typeVoie');
    if (curElement != null && cxfjsutils.isNodeNamedNS(curElement, 'http://www.universite.org/Interfaces/Etudiant/v1/model', 'typeVoie')) {
     var value = null;
     if (!cxfjsutils.isElementNil(curElement)) {
      value = cxfjsutils.getNodeText(curElement);
      item = value;
     }
     newobject.setTypeVoie(item);
     var item = null;
     if (curElement != null) {
      curElement = cxfjsutils.getNextElementSibling(curElement);
     }
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing nomVoie');
    if (curElement != null && cxfjsutils.isNodeNamedNS(curElement, 'http://www.universite.org/Interfaces/Etudiant/v1/model', 'nomVoie')) {
     var value = null;
     if (!cxfjsutils.isElementNil(curElement)) {
      value = cxfjsutils.getNodeText(curElement);
      item = value;
     }
     newobject.setNomVoie(item);
     var item = null;
     if (curElement != null) {
      curElement = cxfjsutils.getNextElementSibling(curElement);
     }
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing complement');
    if (curElement != null && cxfjsutils.isNodeNamedNS(curElement, 'http://www.universite.org/Interfaces/Etudiant/v1/model', 'complement')) {
     var value = null;
     if (!cxfjsutils.isElementNil(curElement)) {
      value = cxfjsutils.getNodeText(curElement);
      item = value;
     }
     newobject.setComplement(item);
     var item = null;
     if (curElement != null) {
      curElement = cxfjsutils.getNextElementSibling(curElement);
     }
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing codePostal');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     value = cxfjsutils.getNodeText(curElement);
     item = value;
    }
    newobject.setCodePostal(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing commune');
    if (curElement != null && cxfjsutils.isNodeNamedNS(curElement, 'http://www.universite.org/Interfaces/Etudiant/v1/model', 'commune')) {
     var value = null;
     if (!cxfjsutils.isElementNil(curElement)) {
      value = cxfjsutils.getNodeText(curElement);
      item = value;
     }
     newobject.setCommune(item);
     var item = null;
     if (curElement != null) {
      curElement = cxfjsutils.getNextElementSibling(curElement);
     }
    }
    return newobject;
}

//
// Definitions for schema: http://www.universite.org/Interfaces/exception
//  file:/C:/FormationMaster/workspace/TP4%20Client%20WS%20SOAP/src/resources/exception.xsd
//
//
// Constructor for XML Schema item {http://www.universite.org/Interfaces/exception}WSErreur
//
function EXCEPTION_WSErreur () {
    this.typeMarker = 'EXCEPTION_WSErreur';
    this._classe = '';
    this._methode = '';
    this._raison = '';
    this._description = null;
}

//
// accessor is EXCEPTION_WSErreur.prototype.getClasse
// element get for classe
// - element type is {http://www.w3.org/2001/XMLSchema}string
// - required element
//
// element set for classe
// setter function is is EXCEPTION_WSErreur.prototype.setClasse
//
function EXCEPTION_WSErreur_getClasse() { return this._classe;}

EXCEPTION_WSErreur.prototype.getClasse = EXCEPTION_WSErreur_getClasse;

function EXCEPTION_WSErreur_setClasse(value) { this._classe = value;}

EXCEPTION_WSErreur.prototype.setClasse = EXCEPTION_WSErreur_setClasse;
//
// accessor is EXCEPTION_WSErreur.prototype.getMethode
// element get for methode
// - element type is {http://www.w3.org/2001/XMLSchema}string
// - required element
//
// element set for methode
// setter function is is EXCEPTION_WSErreur.prototype.setMethode
//
function EXCEPTION_WSErreur_getMethode() { return this._methode;}

EXCEPTION_WSErreur.prototype.getMethode = EXCEPTION_WSErreur_getMethode;

function EXCEPTION_WSErreur_setMethode(value) { this._methode = value;}

EXCEPTION_WSErreur.prototype.setMethode = EXCEPTION_WSErreur_setMethode;
//
// accessor is EXCEPTION_WSErreur.prototype.getRaison
// element get for raison
// - element type is {http://www.w3.org/2001/XMLSchema}string
// - required element
//
// element set for raison
// setter function is is EXCEPTION_WSErreur.prototype.setRaison
//
function EXCEPTION_WSErreur_getRaison() { return this._raison;}

EXCEPTION_WSErreur.prototype.getRaison = EXCEPTION_WSErreur_getRaison;

function EXCEPTION_WSErreur_setRaison(value) { this._raison = value;}

EXCEPTION_WSErreur.prototype.setRaison = EXCEPTION_WSErreur_setRaison;
//
// accessor is EXCEPTION_WSErreur.prototype.getDescription
// element get for description
// - element type is {http://www.w3.org/2001/XMLSchema}string
// - optional element
//
// element set for description
// setter function is is EXCEPTION_WSErreur.prototype.setDescription
//
function EXCEPTION_WSErreur_getDescription() { return this._description;}

EXCEPTION_WSErreur.prototype.getDescription = EXCEPTION_WSErreur_getDescription;

function EXCEPTION_WSErreur_setDescription(value) { this._description = value;}

EXCEPTION_WSErreur.prototype.setDescription = EXCEPTION_WSErreur_setDescription;
//
// Serialize {http://www.universite.org/Interfaces/exception}WSErreur
//
function EXCEPTION_WSErreur_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName !== null) {
     xml = xml + '<';
     xml = xml + elementName;
     xml = xml + ' ';
     xml = xml + 'xmlns:jns0=\'http://www.universite.org/Interfaces/GestionInscriptions/v2\' xmlns:jns1=\'http://www.universite.org/Interfaces/exception\' xmlns:jns2=\'http://www.universite.org/Interfaces/Etudiant/v1/model\' ';
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     xml = xml + '<jns1:classe>';
     xml = xml + cxfjsutils.escapeXmlEntities(this._classe);
     xml = xml + '</jns1:classe>';
    }
    // block for local variables
    {
     xml = xml + '<jns1:methode>';
     xml = xml + cxfjsutils.escapeXmlEntities(this._methode);
     xml = xml + '</jns1:methode>';
    }
    // block for local variables
    {
     xml = xml + '<jns1:raison>';
     xml = xml + cxfjsutils.escapeXmlEntities(this._raison);
     xml = xml + '</jns1:raison>';
    }
    // block for local variables
    {
     if (this._description != null) {
      xml = xml + '<jns1:description>';
      xml = xml + cxfjsutils.escapeXmlEntities(this._description);
      xml = xml + '</jns1:description>';
     }
    }
    if (elementName !== null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

EXCEPTION_WSErreur.prototype.serialize = EXCEPTION_WSErreur_serialize;

function EXCEPTION_WSErreur_deserialize (cxfjsutils, element) {
    var newobject = new EXCEPTION_WSErreur();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing classe');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     value = cxfjsutils.getNodeText(curElement);
     item = value;
    }
    newobject.setClasse(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing methode');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     value = cxfjsutils.getNodeText(curElement);
     item = value;
    }
    newobject.setMethode(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing raison');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     value = cxfjsutils.getNodeText(curElement);
     item = value;
    }
    newobject.setRaison(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing description');
    if (curElement != null && cxfjsutils.isNodeNamedNS(curElement, 'http://www.universite.org/Interfaces/exception', 'description')) {
     var value = null;
     if (!cxfjsutils.isElementNil(curElement)) {
      value = cxfjsutils.getNodeText(curElement);
      item = value;
     }
     newobject.setDescription(item);
     var item = null;
     if (curElement != null) {
      curElement = cxfjsutils.getNextElementSibling(curElement);
     }
    }
    return newobject;
}

//
// Constructor for XML Schema item {http://www.universite.org/Interfaces/exception}WSErreurFonctionelle
//
function EXCEPTION_WSErreurFonctionelle () {
    this.typeMarker = 'EXCEPTION_WSErreurFonctionelle';
    this._classe = '';
    this._methode = '';
    this._raison = '';
    this._description = null;
}

//
// accessor is EXCEPTION_WSErreurFonctionelle.prototype.getClasse
// element get for classe
// - element type is {http://www.w3.org/2001/XMLSchema}string
// - required element
//
// element set for classe
// setter function is is EXCEPTION_WSErreurFonctionelle.prototype.setClasse
//
function EXCEPTION_WSErreurFonctionelle_getClasse() { return this._classe;}

EXCEPTION_WSErreurFonctionelle.prototype.getClasse = EXCEPTION_WSErreurFonctionelle_getClasse;

function EXCEPTION_WSErreurFonctionelle_setClasse(value) { this._classe = value;}

EXCEPTION_WSErreurFonctionelle.prototype.setClasse = EXCEPTION_WSErreurFonctionelle_setClasse;
//
// accessor is EXCEPTION_WSErreurFonctionelle.prototype.getMethode
// element get for methode
// - element type is {http://www.w3.org/2001/XMLSchema}string
// - required element
//
// element set for methode
// setter function is is EXCEPTION_WSErreurFonctionelle.prototype.setMethode
//
function EXCEPTION_WSErreurFonctionelle_getMethode() { return this._methode;}

EXCEPTION_WSErreurFonctionelle.prototype.getMethode = EXCEPTION_WSErreurFonctionelle_getMethode;

function EXCEPTION_WSErreurFonctionelle_setMethode(value) { this._methode = value;}

EXCEPTION_WSErreurFonctionelle.prototype.setMethode = EXCEPTION_WSErreurFonctionelle_setMethode;
//
// accessor is EXCEPTION_WSErreurFonctionelle.prototype.getRaison
// element get for raison
// - element type is {http://www.w3.org/2001/XMLSchema}string
// - required element
//
// element set for raison
// setter function is is EXCEPTION_WSErreurFonctionelle.prototype.setRaison
//
function EXCEPTION_WSErreurFonctionelle_getRaison() { return this._raison;}

EXCEPTION_WSErreurFonctionelle.prototype.getRaison = EXCEPTION_WSErreurFonctionelle_getRaison;

function EXCEPTION_WSErreurFonctionelle_setRaison(value) { this._raison = value;}

EXCEPTION_WSErreurFonctionelle.prototype.setRaison = EXCEPTION_WSErreurFonctionelle_setRaison;
//
// accessor is EXCEPTION_WSErreurFonctionelle.prototype.getDescription
// element get for description
// - element type is {http://www.w3.org/2001/XMLSchema}string
// - optional element
//
// element set for description
// setter function is is EXCEPTION_WSErreurFonctionelle.prototype.setDescription
//
function EXCEPTION_WSErreurFonctionelle_getDescription() { return this._description;}

EXCEPTION_WSErreurFonctionelle.prototype.getDescription = EXCEPTION_WSErreurFonctionelle_getDescription;

function EXCEPTION_WSErreurFonctionelle_setDescription(value) { this._description = value;}

EXCEPTION_WSErreurFonctionelle.prototype.setDescription = EXCEPTION_WSErreurFonctionelle_setDescription;
//
// Serialize {http://www.universite.org/Interfaces/exception}WSErreurFonctionelle
//
function EXCEPTION_WSErreurFonctionelle_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName !== null) {
     xml = xml + '<';
     xml = xml + elementName;
     xml = xml + ' ';
     xml = xml + 'xmlns:jns0=\'http://www.universite.org/Interfaces/GestionInscriptions/v2\' xmlns:jns1=\'http://www.universite.org/Interfaces/exception\' xmlns:jns2=\'http://www.universite.org/Interfaces/Etudiant/v1/model\' ';
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     xml = xml + '<jns1:classe>';
     xml = xml + cxfjsutils.escapeXmlEntities(this._classe);
     xml = xml + '</jns1:classe>';
    }
    // block for local variables
    {
     xml = xml + '<jns1:methode>';
     xml = xml + cxfjsutils.escapeXmlEntities(this._methode);
     xml = xml + '</jns1:methode>';
    }
    // block for local variables
    {
     xml = xml + '<jns1:raison>';
     xml = xml + cxfjsutils.escapeXmlEntities(this._raison);
     xml = xml + '</jns1:raison>';
    }
    // block for local variables
    {
     if (this._description != null) {
      xml = xml + '<jns1:description>';
      xml = xml + cxfjsutils.escapeXmlEntities(this._description);
      xml = xml + '</jns1:description>';
     }
    }
    if (elementName !== null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

EXCEPTION_WSErreurFonctionelle.prototype.serialize = EXCEPTION_WSErreurFonctionelle_serialize;

function EXCEPTION_WSErreurFonctionelle_deserialize (cxfjsutils, element) {
    var newobject = new EXCEPTION_WSErreurFonctionelle();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing classe');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     value = cxfjsutils.getNodeText(curElement);
     item = value;
    }
    newobject.setClasse(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing methode');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     value = cxfjsutils.getNodeText(curElement);
     item = value;
    }
    newobject.setMethode(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing raison');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     value = cxfjsutils.getNodeText(curElement);
     item = value;
    }
    newobject.setRaison(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing description');
    if (curElement != null && cxfjsutils.isNodeNamedNS(curElement, 'http://www.universite.org/Interfaces/exception', 'description')) {
     var value = null;
     if (!cxfjsutils.isElementNil(curElement)) {
      value = cxfjsutils.getNodeText(curElement);
      item = value;
     }
     newobject.setDescription(item);
     var item = null;
     if (curElement != null) {
      curElement = cxfjsutils.getNextElementSibling(curElement);
     }
    }
    return newobject;
}

//
// Constructor for XML Schema item {http://www.universite.org/Interfaces/exception}WSErreurTechnique
//
function EXCEPTION_WSErreurTechnique () {
    this.typeMarker = 'EXCEPTION_WSErreurTechnique';
    this._classe = '';
    this._methode = '';
    this._raison = '';
    this._description = null;
}

//
// accessor is EXCEPTION_WSErreurTechnique.prototype.getClasse
// element get for classe
// - element type is {http://www.w3.org/2001/XMLSchema}string
// - required element
//
// element set for classe
// setter function is is EXCEPTION_WSErreurTechnique.prototype.setClasse
//
function EXCEPTION_WSErreurTechnique_getClasse() { return this._classe;}

EXCEPTION_WSErreurTechnique.prototype.getClasse = EXCEPTION_WSErreurTechnique_getClasse;

function EXCEPTION_WSErreurTechnique_setClasse(value) { this._classe = value;}

EXCEPTION_WSErreurTechnique.prototype.setClasse = EXCEPTION_WSErreurTechnique_setClasse;
//
// accessor is EXCEPTION_WSErreurTechnique.prototype.getMethode
// element get for methode
// - element type is {http://www.w3.org/2001/XMLSchema}string
// - required element
//
// element set for methode
// setter function is is EXCEPTION_WSErreurTechnique.prototype.setMethode
//
function EXCEPTION_WSErreurTechnique_getMethode() { return this._methode;}

EXCEPTION_WSErreurTechnique.prototype.getMethode = EXCEPTION_WSErreurTechnique_getMethode;

function EXCEPTION_WSErreurTechnique_setMethode(value) { this._methode = value;}

EXCEPTION_WSErreurTechnique.prototype.setMethode = EXCEPTION_WSErreurTechnique_setMethode;
//
// accessor is EXCEPTION_WSErreurTechnique.prototype.getRaison
// element get for raison
// - element type is {http://www.w3.org/2001/XMLSchema}string
// - required element
//
// element set for raison
// setter function is is EXCEPTION_WSErreurTechnique.prototype.setRaison
//
function EXCEPTION_WSErreurTechnique_getRaison() { return this._raison;}

EXCEPTION_WSErreurTechnique.prototype.getRaison = EXCEPTION_WSErreurTechnique_getRaison;

function EXCEPTION_WSErreurTechnique_setRaison(value) { this._raison = value;}

EXCEPTION_WSErreurTechnique.prototype.setRaison = EXCEPTION_WSErreurTechnique_setRaison;
//
// accessor is EXCEPTION_WSErreurTechnique.prototype.getDescription
// element get for description
// - element type is {http://www.w3.org/2001/XMLSchema}string
// - optional element
//
// element set for description
// setter function is is EXCEPTION_WSErreurTechnique.prototype.setDescription
//
function EXCEPTION_WSErreurTechnique_getDescription() { return this._description;}

EXCEPTION_WSErreurTechnique.prototype.getDescription = EXCEPTION_WSErreurTechnique_getDescription;

function EXCEPTION_WSErreurTechnique_setDescription(value) { this._description = value;}

EXCEPTION_WSErreurTechnique.prototype.setDescription = EXCEPTION_WSErreurTechnique_setDescription;
//
// Serialize {http://www.universite.org/Interfaces/exception}WSErreurTechnique
//
function EXCEPTION_WSErreurTechnique_serialize(cxfjsutils, elementName, extraNamespaces) {
    var xml = '';
    if (elementName !== null) {
     xml = xml + '<';
     xml = xml + elementName;
     xml = xml + ' ';
     xml = xml + 'xmlns:jns0=\'http://www.universite.org/Interfaces/GestionInscriptions/v2\' xmlns:jns1=\'http://www.universite.org/Interfaces/exception\' xmlns:jns2=\'http://www.universite.org/Interfaces/Etudiant/v1/model\' ';
     if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
     }
     xml = xml + '>';
    }
    // block for local variables
    {
     xml = xml + '<jns1:classe>';
     xml = xml + cxfjsutils.escapeXmlEntities(this._classe);
     xml = xml + '</jns1:classe>';
    }
    // block for local variables
    {
     xml = xml + '<jns1:methode>';
     xml = xml + cxfjsutils.escapeXmlEntities(this._methode);
     xml = xml + '</jns1:methode>';
    }
    // block for local variables
    {
     xml = xml + '<jns1:raison>';
     xml = xml + cxfjsutils.escapeXmlEntities(this._raison);
     xml = xml + '</jns1:raison>';
    }
    // block for local variables
    {
     if (this._description != null) {
      xml = xml + '<jns1:description>';
      xml = xml + cxfjsutils.escapeXmlEntities(this._description);
      xml = xml + '</jns1:description>';
     }
    }
    if (elementName !== null) {
     xml = xml + '</';
     xml = xml + elementName;
     xml = xml + '>';
    }
    return xml;
}

EXCEPTION_WSErreurTechnique.prototype.serialize = EXCEPTION_WSErreurTechnique_serialize;

function EXCEPTION_WSErreurTechnique_deserialize (cxfjsutils, element) {
    var newobject = new EXCEPTION_WSErreurTechnique();
    cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
    var curElement = cxfjsutils.getFirstElementChild(element);
    var item;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing classe');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     value = cxfjsutils.getNodeText(curElement);
     item = value;
    }
    newobject.setClasse(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing methode');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     value = cxfjsutils.getNodeText(curElement);
     item = value;
    }
    newobject.setMethode(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing raison');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
     value = cxfjsutils.getNodeText(curElement);
     item = value;
    }
    newobject.setRaison(item);
    var item = null;
    if (curElement != null) {
     curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing description');
    if (curElement != null && cxfjsutils.isNodeNamedNS(curElement, 'http://www.universite.org/Interfaces/exception', 'description')) {
     var value = null;
     if (!cxfjsutils.isElementNil(curElement)) {
      value = cxfjsutils.getNodeText(curElement);
      item = value;
     }
     newobject.setDescription(item);
     var item = null;
     if (curElement != null) {
      curElement = cxfjsutils.getNextElementSibling(curElement);
     }
    }
    return newobject;
}

//
// Definitions for service: {http://www.universite.org/Interfaces/GestionInscriptions/v2}GererInscriptionSoapService
//

// Javascript for {http://www.universite.org/Interfaces/GestionInscriptions/v2}GererInscriptionSoap

function INTF_GererInscriptionSoap () {
    this.jsutils = new CxfApacheOrgUtil();
    this.jsutils.interfaceObject = this;
    this.synchronous = false;
    this.url = null;
    this.client = null;
    this.response = null;
    this.globalElementSerializers = [];
    this.globalElementDeserializers = [];
    this.globalElementSerializers['{http://www.universite.org/Interfaces/GestionInscriptions/v2}inscrireEtudiant'] = INTF_inscrireEtudiant_serialize;
    this.globalElementDeserializers['{http://www.universite.org/Interfaces/GestionInscriptions/v2}inscrireEtudiant'] = INTF_inscrireEtudiant_deserialize;
    this.globalElementSerializers['{http://www.universite.org/Interfaces/GestionInscriptions/v2}listerInscrits'] = INTF_listerInscrits_serialize;
    this.globalElementDeserializers['{http://www.universite.org/Interfaces/GestionInscriptions/v2}listerInscrits'] = INTF_listerInscrits_deserialize;
    this.globalElementSerializers['{http://www.universite.org/Interfaces/GestionInscriptions/v2}etudiantInexistant'] = INTF_etudiantInexistant_serialize;
    this.globalElementDeserializers['{http://www.universite.org/Interfaces/GestionInscriptions/v2}etudiantInexistant'] = INTF_etudiantInexistant_deserialize;
    this.globalElementSerializers['{http://www.universite.org/Interfaces/GestionInscriptions/v2}InfosEtudiantIncorrect'] = INTF_InfosEtudiantIncorrect_serialize;
    this.globalElementDeserializers['{http://www.universite.org/Interfaces/GestionInscriptions/v2}InfosEtudiantIncorrect'] = INTF_InfosEtudiantIncorrect_deserialize;
    this.globalElementSerializers['{http://www.universite.org/Interfaces/GestionInscriptions/v2}listerInscritsReponse'] = INTF_listerInscritsReponse_serialize;
    this.globalElementDeserializers['{http://www.universite.org/Interfaces/GestionInscriptions/v2}listerInscritsReponse'] = INTF_listerInscritsReponse_deserialize;
    this.globalElementSerializers['{http://www.universite.org/Interfaces/GestionInscriptions/v2}inscrireEtudiantReponse'] = INTF_inscrireEtudiantReponse_serialize;
    this.globalElementDeserializers['{http://www.universite.org/Interfaces/GestionInscriptions/v2}inscrireEtudiantReponse'] = INTF_inscrireEtudiantReponse_deserialize;
    this.globalElementSerializers['{http://www.universite.org/Interfaces/GestionInscriptions/v2}annulerInscription'] = INTF_annulerInscription_serialize;
    this.globalElementDeserializers['{http://www.universite.org/Interfaces/GestionInscriptions/v2}annulerInscription'] = INTF_annulerInscription_deserialize;
    this.globalElementSerializers['{http://www.universite.org/Interfaces/GestionInscriptions/v2}annulerInscriptionReponse'] = INTF_annulerInscriptionReponse_serialize;
    this.globalElementDeserializers['{http://www.universite.org/Interfaces/GestionInscriptions/v2}annulerInscriptionReponse'] = INTF_annulerInscriptionReponse_deserialize;
    this.globalElementSerializers['{http://www.universite.org/Interfaces/Etudiant/v1/model}EtudiantType'] = ETUD_EtudiantType_serialize;
    this.globalElementDeserializers['{http://www.universite.org/Interfaces/Etudiant/v1/model}EtudiantType'] = ETUD_EtudiantType_deserialize;
    this.globalElementSerializers['{http://www.universite.org/Interfaces/Etudiant/v1/model}AdresseType'] = ETUD_AdresseType_serialize;
    this.globalElementDeserializers['{http://www.universite.org/Interfaces/Etudiant/v1/model}AdresseType'] = ETUD_AdresseType_deserialize;
    this.globalElementSerializers['{http://www.universite.org/Interfaces/exception}WSErreurTechnique'] = EXCEPTION_WSErreurTechnique_serialize;
    this.globalElementDeserializers['{http://www.universite.org/Interfaces/exception}WSErreurTechnique'] = EXCEPTION_WSErreurTechnique_deserialize;
    this.globalElementSerializers['{http://www.universite.org/Interfaces/exception}WSErreur'] = EXCEPTION_WSErreur_serialize;
    this.globalElementDeserializers['{http://www.universite.org/Interfaces/exception}WSErreur'] = EXCEPTION_WSErreur_deserialize;
    this.globalElementSerializers['{http://www.universite.org/Interfaces/exception}WSErreurFonctionelle'] = EXCEPTION_WSErreurFonctionelle_serialize;
    this.globalElementDeserializers['{http://www.universite.org/Interfaces/exception}WSErreurFonctionelle'] = EXCEPTION_WSErreurFonctionelle_deserialize;
    this.globalElementSerializers['{http://www.universite.org/Interfaces/exception}WSErreurTechnique'] = EXCEPTION_WSErreurTechnique_serialize;
    this.globalElementDeserializers['{http://www.universite.org/Interfaces/exception}WSErreurTechnique'] = EXCEPTION_WSErreurTechnique_deserialize;
}

function INTF_annulerInscription_op_onsuccess(client, responseXml) {
    if (client.user_onsuccess) {
     var responseObject = null;
     var element = responseXml.documentElement;
     this.jsutils.trace('responseXml: ' + this.jsutils.traceElementName(element));
     element = this.jsutils.getFirstElementChild(element);
     this.jsutils.trace('first element child: ' + this.jsutils.traceElementName(element));
     while (!this.jsutils.isNodeNamedNS(element, 'http://schemas.xmlsoap.org/soap/envelope/', 'Body')) {
      element = this.jsutils.getNextElementSibling(element);
      if (element == null) {
       throw 'No env:Body in message.'
      }
     }
     element = this.jsutils.getFirstElementChild(element);
     this.jsutils.trace('part element: ' + this.jsutils.traceElementName(element));
     this.jsutils.trace('calling INTF_annulerInscriptionReponse_deserializeResponse');
     responseObject = INTF_annulerInscriptionReponse_deserializeResponse(this.jsutils, element);
     client.user_onsuccess(responseObject);
    }
}

INTF_GererInscriptionSoap.prototype.annulerInscription_onsuccess = INTF_annulerInscription_op_onsuccess;

function INTF_annulerInscription_op_onerror(client) {
    if (client.user_onerror) {
     var httpStatus;
     var httpStatusText;
     try {
      httpStatus = client.req.status;
      httpStatusText = client.req.statusText;
     } catch(e) {
      httpStatus = -1;
      httpStatusText = 'Error opening connection to server';
     }
     if (client.parseErrorDetails) {
      client.user_onerror(httpStatus, httpStatusText, client.parseErrorDetails(this));
     } else {
      client.user_onerror(httpStatus, httpStatusText);
     }
    }
}

INTF_GererInscriptionSoap.prototype.annulerInscription_onerror = INTF_annulerInscription_op_onerror;

//
// Operation {http://www.universite.org/Interfaces/GestionInscriptions/v2}annulerInscription
// Wrapped operation.
// parameter id
// - simple type {http://www.w3.org/2001/XMLSchema}string// parameter Nom
// - simple type {http://www.w3.org/2001/XMLSchema}string//
function INTF_annulerInscription_op(successCallback, errorCallback, id, Nom) {
    this.client = new CxfApacheOrgClient(this.jsutils);
    var xml = null;
    var args = new Array(2);
    args[0] = id;
    args[1] = Nom;
    xml = this.annulerInscriptionRequete_serializeInput(this.jsutils, args);
    this.client.user_onsuccess = successCallback;
    this.client.user_onerror = errorCallback;
    var closureThis = this;
    this.client.onsuccess = function(client, responseXml) { closureThis.annulerInscription_onsuccess(client, responseXml); };
    this.client.onerror = function(client) { closureThis.annulerInscription_onerror(client); };
    var requestHeaders = [];
    requestHeaders['SOAPAction'] = '';
    this.jsutils.trace('synchronous = ' + this.synchronous);
    this.client.request(this.url, xml, null, this.synchronous, requestHeaders);
}

INTF_GererInscriptionSoap.prototype.annulerInscription = INTF_annulerInscription_op;

function INTF_annulerInscriptionRequete_serializeInput(cxfjsutils, args) {
    var wrapperObj = new INTF_annulerInscription();
    wrapperObj.setId(args[0]);
    wrapperObj.setNom(args[1]);
    var xml;
    xml = cxfjsutils.beginSoap11Message("xmlns:jns0='http://www.universite.org/Interfaces/GestionInscriptions/v2' xmlns:jns1='http://www.universite.org/Interfaces/exception' xmlns:jns2='http://www.universite.org/Interfaces/Etudiant/v1/model' ");
    // block for local variables
    {
     xml = xml + wrapperObj.serialize(cxfjsutils, 'jns0:annulerInscription', null);
    }
    xml = xml + cxfjsutils.endSoap11Message();
    return xml;
}

INTF_GererInscriptionSoap.prototype.annulerInscriptionRequete_serializeInput = INTF_annulerInscriptionRequete_serializeInput;

function INTF_annulerInscriptionReponse_deserializeResponse(cxfjsutils, partElement) {
}
function INTF_inscrireEtudiant_op_onsuccess(client, responseXml) {
    if (client.user_onsuccess) {
     var responseObject = null;
     var element = responseXml.documentElement;
     this.jsutils.trace('responseXml: ' + this.jsutils.traceElementName(element));
     element = this.jsutils.getFirstElementChild(element);
     this.jsutils.trace('first element child: ' + this.jsutils.traceElementName(element));
     while (!this.jsutils.isNodeNamedNS(element, 'http://schemas.xmlsoap.org/soap/envelope/', 'Body')) {
      element = this.jsutils.getNextElementSibling(element);
      if (element == null) {
       throw 'No env:Body in message.'
      }
     }
     element = this.jsutils.getFirstElementChild(element);
     this.jsutils.trace('part element: ' + this.jsutils.traceElementName(element));
     this.jsutils.trace('calling INTF_inscrireEtudiantReponse_deserializeResponse');
     responseObject = INTF_inscrireEtudiantReponse_deserializeResponse(this.jsutils, element);
     client.user_onsuccess(responseObject);
    }
}

INTF_GererInscriptionSoap.prototype.inscrireEtudiant_onsuccess = INTF_inscrireEtudiant_op_onsuccess;

function INTF_inscrireEtudiant_op_onerror(client) {
    if (client.user_onerror) {
     var httpStatus;
     var httpStatusText;
     try {
      httpStatus = client.req.status;
      httpStatusText = client.req.statusText;
     } catch(e) {
      httpStatus = -1;
      httpStatusText = 'Error opening connection to server';
     }
     if (client.parseErrorDetails) {
      client.user_onerror(httpStatus, httpStatusText, client.parseErrorDetails(this));
     } else {
      client.user_onerror(httpStatus, httpStatusText);
     }
    }
}

INTF_GererInscriptionSoap.prototype.inscrireEtudiant_onerror = INTF_inscrireEtudiant_op_onerror;

//
// Operation {http://www.universite.org/Interfaces/GestionInscriptions/v2}inscrireEtudiant
// Wrapped operation.
// parameter etudiant
// - Object constructor is ETUD_EtudiantType
//
function INTF_inscrireEtudiant_op(successCallback, errorCallback, etudiant) {
    this.client = new CxfApacheOrgClient(this.jsutils);
    var xml = null;
    var args = new Array(1);
    args[0] = etudiant;
    xml = this.inscrireEtudiantRequete_serializeInput(this.jsutils, args);
    this.client.user_onsuccess = successCallback;
    this.client.user_onerror = errorCallback;
    var closureThis = this;
    this.client.onsuccess = function(client, responseXml) { closureThis.inscrireEtudiant_onsuccess(client, responseXml); };
    this.client.onerror = function(client) { closureThis.inscrireEtudiant_onerror(client); };
    var requestHeaders = [];
    requestHeaders['SOAPAction'] = '';
    this.jsutils.trace('synchronous = ' + this.synchronous);
    this.client.request(this.url, xml, null, this.synchronous, requestHeaders);
}

INTF_GererInscriptionSoap.prototype.inscrireEtudiant = INTF_inscrireEtudiant_op;

function INTF_inscrireEtudiantRequete_serializeInput(cxfjsutils, args) {
    var wrapperObj = new INTF_inscrireEtudiant();
    wrapperObj.setEtudiant(args[0]);
    var xml;
    xml = cxfjsutils.beginSoap11Message("xmlns:jns0='http://www.universite.org/Interfaces/GestionInscriptions/v2' xmlns:jns1='http://www.universite.org/Interfaces/exception' xmlns:jns2='http://www.universite.org/Interfaces/Etudiant/v1/model' ");
    // block for local variables
    {
     xml = xml + wrapperObj.serialize(cxfjsutils, 'jns0:inscrireEtudiant', null);
    }
    xml = xml + cxfjsutils.endSoap11Message();
    return xml;
}

INTF_GererInscriptionSoap.prototype.inscrireEtudiantRequete_serializeInput = INTF_inscrireEtudiantRequete_serializeInput;

function INTF_inscrireEtudiantReponse_deserializeResponse(cxfjsutils, partElement) {
}
function INTF_listerInscrits_op_onsuccess(client, responseXml) {
    if (client.user_onsuccess) {
     var responseObject = null;
     var element = responseXml.documentElement;
     this.jsutils.trace('responseXml: ' + this.jsutils.traceElementName(element));
     element = this.jsutils.getFirstElementChild(element);
     this.jsutils.trace('first element child: ' + this.jsutils.traceElementName(element));
     while (!this.jsutils.isNodeNamedNS(element, 'http://schemas.xmlsoap.org/soap/envelope/', 'Body')) {
      element = this.jsutils.getNextElementSibling(element);
      if (element == null) {
       throw 'No env:Body in message.'
      }
     }
     element = this.jsutils.getFirstElementChild(element);
     this.jsutils.trace('part element: ' + this.jsutils.traceElementName(element));
     this.jsutils.trace('calling INTF_listerInscritsReponse_deserializeResponse');
     responseObject = INTF_listerInscritsReponse_deserializeResponse(this.jsutils, element);
     client.user_onsuccess(responseObject);
    }
}

INTF_GererInscriptionSoap.prototype.listerInscrits_onsuccess = INTF_listerInscrits_op_onsuccess;

function INTF_listerInscrits_op_onerror(client) {
    if (client.user_onerror) {
     var httpStatus;
     var httpStatusText;
     try {
      httpStatus = client.req.status;
      httpStatusText = client.req.statusText;
     } catch(e) {
      httpStatus = -1;
      httpStatusText = 'Error opening connection to server';
     }
     if (client.parseErrorDetails) {
      client.user_onerror(httpStatus, httpStatusText, client.parseErrorDetails(this));
     } else {
      client.user_onerror(httpStatus, httpStatusText);
     }
    }
}

INTF_GererInscriptionSoap.prototype.listerInscrits_onerror = INTF_listerInscrits_op_onerror;

//
// Operation {http://www.universite.org/Interfaces/GestionInscriptions/v2}listerInscrits
// Wrapped operation.
//
function INTF_listerInscrits_op(successCallback, errorCallback) {
    this.client = new CxfApacheOrgClient(this.jsutils);
    var xml = null;
    var args = new Array(0);
    xml = this.listerInscritsRequete_serializeInput(this.jsutils, args);
    this.client.user_onsuccess = successCallback;
    this.client.user_onerror = errorCallback;
    var closureThis = this;
    this.client.onsuccess = function(client, responseXml) { closureThis.listerInscrits_onsuccess(client, responseXml); };
    this.client.onerror = function(client) { closureThis.listerInscrits_onerror(client); };
    var requestHeaders = [];
    requestHeaders['SOAPAction'] = '';
    this.jsutils.trace('synchronous = ' + this.synchronous);
    this.client.request(this.url, xml, null, this.synchronous, requestHeaders);
}

INTF_GererInscriptionSoap.prototype.listerInscrits = INTF_listerInscrits_op;

function INTF_listerInscritsRequete_serializeInput(cxfjsutils, args) {
    var wrapperObj = new INTF_listerInscrits();
    var xml;
    xml = cxfjsutils.beginSoap11Message("xmlns:jns0='http://www.universite.org/Interfaces/GestionInscriptions/v2' xmlns:jns1='http://www.universite.org/Interfaces/exception' xmlns:jns2='http://www.universite.org/Interfaces/Etudiant/v1/model' ");
    // block for local variables
    {
     xml = xml + wrapperObj.serialize(cxfjsutils, 'jns0:listerInscrits', null);
    }
    xml = xml + cxfjsutils.endSoap11Message();
    return xml;
}

INTF_GererInscriptionSoap.prototype.listerInscritsRequete_serializeInput = INTF_listerInscritsRequete_serializeInput;

function INTF_listerInscritsReponse_deserializeResponse(cxfjsutils, partElement) {
    var returnObject = INTF_listerInscritsReponse_deserialize (cxfjsutils, partElement);

    return returnObject;
}
function INTF_GererInscriptionSoap_INTF_GererInscription () {
  this.url = 'http://localhost:8080/monappli/services/gestioninscription/v2';
}
INTF_GererInscriptionSoap_INTF_GererInscription.prototype = new INTF_GererInscriptionSoap;

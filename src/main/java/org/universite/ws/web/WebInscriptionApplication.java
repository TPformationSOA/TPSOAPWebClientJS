package org.universite.ws.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
//export org.springframework.web.bind.annotation.CrossOrigin;


@SpringBootApplication

//@CrossOrigin
public class WebInscriptionApplication {

	public static void main(String[] args) {
		SpringApplication.run(WebInscriptionApplication.class, args);
	}
}

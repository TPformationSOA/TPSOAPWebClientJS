# Penser à rajouter le proxy pour maven dans NetBeans
-Dhttp.proxyHost=193.49.48.244 -Dhttp.proxyPort=3128

# Récupérer cette application web sur votre poste
 
    cd C:\tmp\projets
    git clone https://framagit.org/TPformationSOA/TPSOAPWebClientJS.git
    
# Créer le projet Maven dans NetBeans

* Utiliser le projet précédement récupéré

* Vérifier le contenu du pom

Nous n'allons plus générer un stub Java mais JavaScript : `wsdl2js`

* Lancer un build du projet

Consulter le contenu des sources générés, à quoi correspond le code généré ?

Consulter application.yml pour connaitre le port d'écoute de l'application web ainsi que l'url de base de l'application (context path)

* Consulter le contenu static

Nous retrouvons le fichier JavaScript généré par `wsdl2js` GererInscriptionSoapService.js et une librairie de fonctions
utilitaires fournis par cxf cxf-utils.

Nous avons deux pages web : index.html et CallSoapFromJS.html.

Quel est l'objet de ces 2 pages html ? quelle différence voyez vous ?

* Lancer l'application et se connecter depuis le navigateur

Assurez vous d'avoir lancé le serveur TPSOAPServer, vérifier son bon fonctionnement depuis SOAP-UI ou directement depuis le navigateur.

Accéder à l'application web et tenter d'invoquer le service en précisant l'url correspondant à votre serveur, ou à celui de l'enseignant si votre 
serveur ne fonctionne pas.

Que constatez-vous ?

Ouvrir dans les options du navigateur la fenêtre Développement pour consulter les requêtes transmises au serveur.

Que voyez vous ? Pourquoi de telles requêtes ? quelles solutions pouvons-nous apporter pour corriger le problème ?
